package toyindexer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;

public class ToyIndexer {

    public static void main(String[] args) {
        String text = readFile("src/toyindexer/documents.txt");
        ArrayList idDoc = new ArrayList();
        ArrayList documents = new ArrayList();
        text = text.replaceAll("<doc><docno>", "");
        text = text.replaceAll("'", "");
        text = text.replaceAll("\\\n", " ");
        text = text.replaceAll("\\\r", " ");
        text = text.replaceAll("\\.", "");
        text = text.replaceAll(",", "");
        text = text.replaceAll(":", "");
        text = text.replaceAll(";", "");
        text = text.replaceAll("#", "");
        while (text.contains("  ")) {
            text = text.replaceAll("  ", " ");
        }
        String docs[] = text.split("</doc>");
        for (String doc : docs) {
            String[] temp = doc.split("</docno>");
            idDoc.add(temp[0]);
            documents.add(temp[1].toLowerCase().replaceAll("’s", " is").replaceAll("’", " is").replaceAll("(?:-0-|[\\[\\]{}()+/\\\\])", ""));
        }
        ArrayList words = new ArrayList();
        for (int i = 0; i < documents.size(); i++) {
            String temp = "" + documents.get(i);
            String[] tokensTmp = temp.split(" ");
            words.addAll(Arrays.asList(tokensTmp));
        }

        Set<String> sorted = new TreeSet<>(words);
        for (String key : sorted) {
            if ("".equals(key)) {
                continue;
            }
            int df;
            String result;
            String afterKey = "";
            int countFre = 0;
            for (int i = 0; i < documents.size(); i++) {
                int count = 0;
                Pattern pattern = Pattern.compile("\\b+" + key);
                Matcher matcher = pattern.matcher("" + documents.get(i));
                while (matcher.find()) {
                    count++;
                }
                if (count != 0) {
                    countFre++;
                    afterKey += "\t" + count + " " + idDoc.get(i) + "\n";
                }
            }
            System.out.println(countFre + "=df(" + key + ")");
            System.out.print(afterKey);
        }
    }

    public static String readFile(String fileName) {
        String text = "";
        String line;
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((line = bufferedReader.readLine()) != null) {
                text += line;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ToyIndexer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ToyIndexer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return text;
    }
}
